Rails.application.routes.draw do

  # resources :employees
  devise_for :companies
  root to: "departments#index"

  resources :departments do
    resources :employees
  end 
     # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
